import Vue from 'vue'
import Router from 'vue-router'
import Game from '@/components/Game'
import LeaderBoard from '@/score/LeaderBoard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Game',
      component: Game
    },
    {
      path: '/score',
      name: 'LeaderBoard',
      component: LeaderBoard
    }
  ]
})
